<?php

namespace artbyrab\tego;

/**
 * Data audit interface
 *
 * A data audit is a timely review of the your data policy which may include
 * but is not limited to:
 *  - Review data policy
 *  - Review data security
 *
 * @author artbyrab
 */
interface DataAuditInterface
{
    /**
     * Get review frequency
     *
     * @return string For example but not limited to:
     *  - 'Monthly'
     *  - 'Quarterly'
     *  - 'Yearly'
     */
    public function getFrequency(): string;

    /**
     * Get review date
     *
     * @return string For example but not limited to
     *  - '1st day of the quarter'
     *  - '20th December'
     *  - 'First monday of every month'
     */
    public function getDate(): string;

    /**
     * Get items
     *
     * This should represent the checks or steps that should be performed in
     * order to complete the audit.
     *
     * @return array This can a simple array of string or you could return
     * an array of objects that contain more attributes and information to
     * each point. The choice is up to you how much detail to return.
     */
    public function getItems(): array;
}
