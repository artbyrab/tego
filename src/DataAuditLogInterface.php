<?php

namespace artbyrab\tego;

/**
 * Data audit log interface
 *
 * A data audit log is a record to show that a data audit has been performed.
 *
 * @author artbyrab
 */
interface DataAuditLogInterface
{
    /**
     * Get review date
     *
     * @return string For example but not limited to
     *  - '1st day of the quarter'
     *  - '20th December'
     *  - 'First monday of every month'
     */
    public function getDate(): string;

    /**
     * Get the status
     *
     * @return string|boolean For example but not limited to:
     *  - 'Pass'
     *  - 'Fail'
     *  - true
     * Or a boolean false if not required.
     */
    public function getStatus();

    /**
     * Get items
     *
     * @return string|boolean Notes on the outcome of the data audit if requred
     * or a boolean false.
     */
    public function getNotes();
}
