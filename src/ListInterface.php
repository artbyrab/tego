<?php

namespace artbyrab\tego;

/**
 * List interface
 *
 * A list is exactly that a list of items that should be adhered to
 * or followed in order to meet a standard.
 *
 * For example the list interface could be used to create a GDPR
 * compliance list or a data security list with a focus on storing
 * data securly.
 *
 * @author artbyrab
 */
interface ListInterface
{
    /**
     * Get title
     *
     * @return string The title of the list for example:
     *  - 'Email security list'
     *  - 'Data security list'
     *  - 'Third party service list'
     */
    public function getTitle(): string;

    /**
     * Get description
     *
     * @return string The description of the list for example:
     *  - 'This list provides items related to email security. This is
     *  specifically related to internal email policy and does not affect users
     *  who are not part of the company group.'
     */
    public function getDescription(): string;

    /**
     * Get items
     *
     * @return array This can a simple array of string or you could return
     * an array of objects that contain more attributes and information to
     * each point. The choice is up to you how much detail to return.
     */
    public function getItems(): array;
}
