<?php

namespace artbyrab\tego;

/**
 * Data report interface
 *
 * A data report is the report that is run and sent to the entity requesting the
 * data. For example if a user contacts us and asks for a record of all the data
 * we hold on them then we need to send the the data. In that case then this
 * report might contain emails, notes that pertain to the requestee.
 *
 * @author artbyrab
 */
interface DataReportInterface
{
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Get data sources
     *
     * @return array An array of objects that implement the DataSourceInterface
     * interface.
     */
    public function getDataSources(): array;

    /**
     * Get data
     *
     * @return array An array of objects implementing the DataRecord interface.
     */
    public function getData(): array;
}
