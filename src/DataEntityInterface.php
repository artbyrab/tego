<?php

namespace artbyrab\tego;

/**
 * Data entity interface
 *
 * A data entity can represent an individual or group you may have data on.
 * For example if your app has user accounts where users login to your app,
 * then one of your data entities would be your user accounts.
 *
 * @author artbyrab
 */
interface DataEntityInterface
{
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Get data sources
     *
     * @return array An array of objects that implement the DataSourceInterface
     * interface.
     */
    public function getDataSources(): array;
}
