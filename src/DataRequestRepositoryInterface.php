<?php

namespace artbyrab\tego;

/**
 * Data request repository interface
 *
 * This class will provide repository functionality relating to data requests.
 *
 * @author artbyrab
 */
interface DataRequestRepositoryInterface
{
    /**
     * Find
     *
     * @return array An array of objects that implement the DataRequest
     * interface.
     */
    public static function find(): array;

    /**
     * Find open
     *
     * @return array An array of objects that implement the DataRequest
     * interface.
     */
    public static function findOpen(): array;

    /**
     * Find closed
     *
     * @return array An array of objects that implement the DataRequest
     * interface.
     */
    public static function findClosed(): array;
}
