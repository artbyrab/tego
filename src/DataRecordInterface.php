<?php

namespace artbyrab\tego;

/**
 * Data record interface
 *
 * When a requestee makes a data request, we create a data report for them.
 * The data in the data report is made up from one of more objects that
 * implement this interface.
 *
 * @author artbyrab
 */
interface DataRecordInterface
{
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Get data
     *
     * @return string The data as a string.
     */
    public function getData(): string;
}
