<?php

namespace artbyrab\tego;

/**
 * @todo review if this is actually needed
 *
 * Data source interface
 *
 * A data source is a point in your system where you might store personally
 * identifying information about an entity. For example if you have users that
 * register to use your app you might have the following data sources:
 *  - User accounts
 *      - database records per user which contain email, telephone and address
 *      details
 *  - Emails related to users
 *      - Emails that have been sent directly to users from your staff
 *
 * Therefore in the event of a data request by a user you might need to provide
 * them both the details of their user account and also the emails you have
 * stored in sent outboxes.
 *
 * This would require that you create 2 data sources:
 *  - User account
 *  - Send emails
 *
 * @author artbyrab
 */
interface DataSourceInterface
{
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string;
}
