<?php

namespace artbyrab\tego;

use artbyrab\tego\VerificationRequestInterface;
use artbyrab\tego\DataReportInterface;

/**
 * Data request interface
 *
 * A data request is a request related to data made by an entity.
 *
 * A request might be one of the following:
 *  - Data information request
 *      - What information you might hold on an individual
 *  - Data deletion request
 *      - A user requesting the GDPR's right to be forgotten
 *
 * This class will provide a generic interface to build requests. For specific
 * functionality this class can be extended.
 *
 * @todo this class should at a minimum have the requestee details and the
 * type of request.
 *
 * @author artbyrab
 */
interface DataRequestInterface
{
    /**
     * Find
     *
     * @param string|integer $id
     * @return object|null
     */
    public static function find($id);

    /**
     * Request verification
     *
     * Request additional verification information from the data request
     * requestee.
     *
     * @param object $verificationRequest
     */
    public function requestVerification(
        VerificationRequestInterface $verificationRequest
    );

    /**
     * Send data report
     *
     * Send a data report to a requestee.
     *
     * @param object $dataReport
     */
    public function sendDataReport(DataReport $dataReport);

    /**
     * Delete data
     */
    public function deleteData();

    /**
     * Send data deletion confirmation
     *
     * Send a data deletion confirmation to the data requestee.
     *
     * @param object $dataReport
     */
    public function sendDataDeletionConfirmation(DataReport $dataReport);

    /**
     * Refuse
     *
     * @param string $reason
     */
    public function refuse(string $reason);
}
