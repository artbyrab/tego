<?php

namespace artbyrab\tego;

/**
 * Data personnel interface
 *
 * This will represent a data personnel
 *
 * @author artbyrab
 */
interface DataPersonnelInterface
{
    /**
     * Get title
     *
     * For example:
     *  - 'Data Protection Officer'
     *  - 'Head of data'
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get email
     *
     * @return string|false
     */
    public function getEmail();

    /**
     * Get phone number
     *
     * @return string|false
     */
    public function getPhoneNumber();
}
