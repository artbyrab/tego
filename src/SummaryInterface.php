<?php

namespace artbyrab\tego;

/**
 * Summary interface
 *
 * This interface will aggrogate all your classes into a single class that
 * provides a summary of whatever classes you implemented.
 *
 * @author artbyrab
 */
interface SummaryInterface
{
    /**
     * Get documents
     *
     * This can include both the pbulci and internal documents.
     *
     * @return array|false
     */
    public function getDocuments();

    /**
     * Get public documents
     *
     * Documents that are intended for public consumption.
     *
     * @return array|false
     */
    public function getPublicDocuments();

    /**
     * Get internal documents
     *
     * Document that are internal only for internal staff or agreed third
     * parties.
     *
     * @return array|false
     */
    public function getInternalDocuments();

    /**
     * Get data regulations
     *
     * The data regulations that are adhere to or observed.
     *
     * @return array|false
     */
    public function getDataRegulations();

    /**
     * Get lists
     *
     * @return array|false
     */
    public function getLists();

    /**
     * Get contact information
     *
     * @return array|false
     */
    public function getContactInformation();

    /**
     * Get data entities
     *
     * @return array|false
     */
    public function getDataEntities();

    /**
     * Get personnel
     *
     * @return array|false
     */
    public function getPersonnel();

    /**
     * Get checklists
     *
     * @return array|false
     */
    public function getDataSources();

    /**
     * Get checklists
     *
     * @return array|false
     */
    public function getDisasterRecoveryPlans();

    /**
     * Get audit
     *
     * @return object|false An object implementing the DataAuditInterface
     * or a boolean false.
     */
    public function getAudit();

    /**
     * Get audit logs
     *
     * @return array|false An array of objects implementing the
     * DataAuditLogInterface or a boolean false.
     */
    public function getAuditLogs();
}
