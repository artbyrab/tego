<?php

namespace artbyrab\tego;

/**
 * Data personnel repository interface
 *
 * This class will provide repository functionality relating to data personnel.
 *
 * @author artbyrab
 */
interface DataPersonnelRepositoryInterface
{
    /**
     * Find
     *
     * @return array An array of objects that implements the
     * DataPersonnelInterface.
     */
    public static function find(): array;
}
