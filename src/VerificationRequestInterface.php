<?php

namespace artbyrab\tego;

/**
 * Verification request interface
 *
 * When an entity makes a data request there may be a need to verify the
 * requestee to ensure they should be allowed to make a data request. In that
 * case you use use the verification request interface to create a object that
 * would be used when requesting verification.
 *
 * @author artbyrab
 */
interface VerificationRequestInterface
{
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get instructions
     *
     * @return array;
     */
    public function getInstructions(): array;
}
