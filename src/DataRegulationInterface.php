<?php

namespace artbyrab\tego;

/**
 * Data regulation interface
 *
 * This is a class designed to represent a data regulation. For example the
 * GDPR in Europe or the California Consumer Privacy Act (CCPA) in California.
 *
 * @author artbyrab
 */
interface DataRegulationInterface
{
    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Get URL
     *
     * @return string
     */
    public function getUrl(): string;
}
