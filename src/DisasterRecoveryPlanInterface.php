<?php

namespace artbyrab\tego;

use artbyrab\tego\ListInterface;

/**
 * Disaster recovery plan interface
 *
 * This call represents a disaster recovery plan that is related to data.
 *
 * You can implement this interface for a single disaster recovery plan for
 * everything or you can also implement multiple disaster recovery plans that
 * cover more focused topics. If you choose to have multiple disaster recovery
 * plans that can be added or grouped into a disaster recovery plan repository.
 *
 * @todo review if this class is really needed as it does nothing different
 * than its parent ListInterface.
 *
 * @author artbyrab
 */
interface DisasterRecoveryPlanInterface extends ListInterface
{
}
