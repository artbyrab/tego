<?php

namespace artbyrab\tego;

/**
 * Contact information interface
 *
 * This class can be used to define your contact information relating to data.
 * For example what email should be used for people wishing to make data
 * requests with your business or app?
 *
 * When you extend this class you can of course add any other methods like
 * a phone number.
 *
 * @author artbyrab
 */
interface ContactInformationInterface
{
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail(): string;

    /**
     * Get data request email
     *
     * @return string
     */
    public function getDataRequestEmail(): string;

    /**
     * Get address
     *
     * @return string|false The address in case a user wishes to make a
     * data request via mail or boolean false.
     */
    public function getAddress();
}
