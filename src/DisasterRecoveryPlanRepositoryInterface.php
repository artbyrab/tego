<?php

namespace artbyrab\tego;

/**
 * Data recovery plan repository interface
 *
 * This class will provide repository functionality relating to disaster
 * recovery plans.
 *
 * @author artbyrab
 */
interface DisasterRecoveryPlanRepositoryInterface
{
    /**
     * Find
     *
     * @return array An array of objects that implement the
     * DisasterRecoveryPlan interface.
     */
    public static function find(): array;
}
