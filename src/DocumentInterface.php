<?php

namespace artbyrab\tego;

/**
 * Document interface
 *
 * A document can extend this class. For example this class might be
 * implemented to create a class that represents a privacy policy document
 * or a terms and conditions document.
 *
 * This interface is intended to be used whether a document is a text file
 * like a Word doc or a URL page or flat content like markdown or HTML. Choose
 * the get method that suits you best.
 *
 * @author artbyrab
 */
interface DocumentInterface
{
    /**
     * Get title
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription(): string;

    /**
     * Get content
     *
     * If the content is in Markdown or HTML you can use this to get it.
     *
     * @return string|boolean
     */
    public function getContent();

    /**
     * Get path
     *
     * @return string|boolean The full path to the file including the filename
     * or boolean false.
     */
    public function getPath();

    /**
     * Get URL
     *
     * @return string|boolean
     */
    public function getUrl();

    /**
     * Get location
     *
     * @return string|boolean The location of the file for example if it stored
     * in a folder on a cloud document service you might have the following:
     *  - "Stored on Microsoft 365 cloud"
     */
    public function getLocation();
}
