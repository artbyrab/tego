# Data regulations

This guide will contain information related to data regulations.

## Resources

* General Data Protection Regulation (GDPR)
    * https://en.wikipedia.org/wiki/General_Data_Protection_Regulation
* California Consumer Privacy Act (CCPA)
    * https://en.wikipedia.org/wiki/California_Consumer_Privacy_Act
*  UK ICO
    * https://en.wikipedia.org/wiki/Information_Commissioner%27s_Office