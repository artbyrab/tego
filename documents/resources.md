# Resources

* General Data Protection Regulation (GDPR)
    * https://en.wikipedia.org/wiki/General_Data_Protection_Regulation
    * The right to be forgotten under the GDPR
        * https://gdpr.eu/right-to-be-forgotten/
* California Consumer Privacy Act (CCPA)
    * https://en.wikipedia.org/wiki/California_Consumer_Privacy_Act
* UK ICO
    * https://en.wikipedia.org/wiki/Information_Commissioner%27s_Office
* Data protection officer(DPO)
    * https://en.wikipedia.org/wiki/Data_protection_officer
* Data security
    * https://en.wikipedia.org/wiki/Data_security
* Disaster recovery
    * https://en.wikipedia.org/wiki/Disaster_recovery