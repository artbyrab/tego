# Usage

Below are the steps in order to use Tego.

* Determine which interfaces you would like to implement in your project
* Create classes to implement your chosen interfaces
* Integrate the classes with your existing system

For more detailed usage examples of Tego you can view the Tego Example repo.