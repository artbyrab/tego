# Glossary

Below are various words and phrases related to the library and their meaning. Please note many terminologies are lifted from Wikipdedia.

* Repository
    * A group of data
    * Typically used to describe a class where you can get one or more of an object
* Contact information
    * Information for entities to contact you regarding data
* Data
    * Units of information or in the case of data protection, data related to an entity
    * Typically describing something identifying like an email, name, telephone number or address, but not limited to
* Data deletion request
    * A request from an entity to delete any data you hold on them
* Data entity
    * A individual or group that may have data 
    * For example a user to an app would be considered a data entity
* Data identifier
    * See identifier
* Data information request
    * A request from an entity to provide the data you hold on them
* Data officer
    * AKA Data protection officer(DPO)
    * A data officer is responsible for ensuring an organisation applies the laws and regulations relating to data protection for entities and individuals
* Data protection officer
    * See Data officer
* Data regulation
    * A regulation on data
    * An act or a rule of controlling data 
* Data request
    * A request for data
* Data request requestee
    * The person that made the data request
* Data security
    * The protection of digital information(data)
    * An example would be ensuring the security of data in a database
* Data security checklist
    * A list of points that ensure data security
* Deletion request
    * A request to have data deleted for an entity
    * An example would be the right to be forgotten in the GDPR
* Disaster recovery
    * A set of policies, tools or procedures to work towards the recovery of IT technology, infrastructure or systems in the event of a disaster
* Disaster recovery plan
    * A plan to provide guidance, steps or procedures in the event of a disaster
* Entity
    * An individual or group
    * See Data Entity
* Identifier
    * A piece of data that could be related to an entity.
* Information request
    * See data information request
* Interface
    * Interfaces provide a way to define functionality for a class that must be implemented if the interface is used
* List
    * A list of information that can be used to minimize verbosity and make things easier to read and digest
* Requestee
    * A entity that is making a data request
* Right to be forgotten
    * The right to be forgotten is a rule in the GDPR that allows users the right to have their information deleted
* Verification 
    * A step to ensure entities are who they say they are
* Verification request
    * A request to an entity 