# Implementing interfaces

This guide will provide more information and examples on implementing some of the interfaces in Tego.

## Implement an interface

Choose which Tego interfaces to implement in your app or project. For example if you want to create a data password security checklist you can implement the  ListInterface interface.

* Create a new file in your app for example:
    * DataSecurityList.php
* In the file create the following:

```php
<?php

use artbyrab\tego\ListInterface;

/**
 * Data password storage security checklist
 * 
 * This checklist will list items related to data storage security.
 */
class DataPasswordStorageSecurityList implements ListInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return "Data security checklist";
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return "This checklist will provide our basic data security guidelines. 
        For more detailed information on any of the items please consult the 
        primary data officer.";
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return [
            'Passwords stored in the database are to be hashed with either Argon2, Scrypt or Bcrypt',
            'Passwords stored in a database should be salted as well as hashed',
        ];
    }
}
```