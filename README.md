# Tego

![Image](files/graphics/tego-logo-small.png?raw=true)

Tego is a set of PHP interfaces designed to help you adhere to data regulations in a PHP app. Tego does not provide any code but instead provides guidance with structure on the things you might want to consider when working with data. You can pick and choose what parts of Tego you want to use to assist in building up your data regulation functionality.

Tego was designed to assist with regulations like the GDPR. However, Tego is not limited to the GDPR and can be used with any data regulation and is effectively regulation agnostic.

Tego can help with you define things in your own app or project like:

* Data personnel
    * Who in your app/business will handle data regulations
    * If you have a Data Protection Officer who are they
* Disaster recovery plans
    * How will data disasters be handled in the event one happens
* Data requests
    * If the event that entities make data requests how they will be handled
        * Data information requests
        * Data deletion requests
* Data entities
    * What individuals or groups are affected by your data

And much much more.

## Tego is NOT legal or data regulation advice

Tego will NOT do the job of data regulation for you but it can provide a nice starting point for you to manage your own data regulation. You should always do you own due diligence regarding data regulations. You should read and understand the data regulations in your country and the data regulations that may be applicable to your users. Using Tego in your app or project does not make you compliant with any particular data regulation. Using Tego is not a suitable replacement for understanding the law relating to data regulations. Tego is not legal advice or legal authority on data regulation and should only be used at your own risk and discretion.

## What does Tego mean?

Tego is a Latin word and in English its meaning is to cover, protect and defend.

## Requirements

* PHP

## Features

* Provides interfaces for covering various data regulations

## Installation

The reccomended way to install is via Composer.

Either install in the project via terminal:

```shell
$ composer require artbyrab/tego:~1.0
```

or add it to your composer.json file:

```shell
"artbyrab/tego": "~1.0"
```

## Usage

Choose which Tego interfaces to implement in your app or project. For example if you want to create a data password security checklist you can implement the  ListInterface interface.

* Create a new file in your app for example:
    * DataSecurityList.php
* In the file create the following:

```php
<?php

use artbyrab\tego\ListInterface;

/**
 * Data password storage security checklist
 * 
 * This checklist will list items related to data storage security.
 */
class DataPasswordStorageSecurityList implements ListInterface
{
    /**
     * {@inheritdoc}
     */
    public function getTitle()
    {
        return "Data security checklist";
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return "This checklist will provide our basic data security guidelines. 
        For more detailed information on any of the items please consult the 
        primary data officer.";
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return [
            'Passwords stored in the database are to be hashed with either Argon2, Scrypt or Bcrypt',
            'Passwords stored in a database should be salted as well as hashed',
        ];
    }
}
```

This information can now for example be used in your app or library's admin dashboard. 

For more detailed usage information and examples see the full usage guide.

### 1) Include tego into your app:

```php
// include the inferface you require
use artbyrab\tego\DataEntityInterface;

// Your class here
```

# Resources

* [Code Checks](documents/code-checks.md)
* [Data Regulations](documents/data-regulations.md)
* [Glossary](documents/glossary.md)
* [Implementing Interfaces](documents/implementing-interfaces.md)
* [PHP Compatability Checks](documents/php-compatability-checks.md)
* [PHP CS](documents/php-cs.md)
* [Prettier](documents/prettier.md)
* [Resources](documents/resources.md)
* [Running Tests](documents/running-tests.md)
* [Usage](documents/usage.md)